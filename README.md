# Electron JS App & Redis Chat

An app to reduce the reduntant issues on JIRA interface by providing the solution of an issue before raising on JIRA, primary protocol: Similar tickets & Google Custom Search Engine results & secondary protocol: Redis Chat

## Implementation
* This app is developed to reduce the repetition of issues on JIRA
* User who wants to raise an issue can first navigate here & enter the details of the issue, the app will generate similar issues using (NLP+Jaccard Index+ES) & relevant links using google custom search API which might act as the primary factor of solving the issue rather than raising it on JIRA.
* As a secondary factor, the user could navigate to Redis Chat by clicking against the name of a particular user from comment(s) of any issue and can directly connect with him/her to get the issue resolved.
* For the Redis Chat, Sprinklr members info are extracted using Graph API of Microsoft teams. 

### Additional usage of Redis
* Caching is opted using Redis
* Repetitive ES queries & API results (Graph API, Jira API) are cached 

## Run Locally

Clone the project

```bash
  git clone https://link-to-project
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm install
```

Start server
```bash
  npm start
```

Go to client directory

```bash
  cd client
```

Install node modules

```bash
  npm install
```

Start interface
```bash
  npm start
```

## Screenshots

![App Screenshot](https://i.ibb.co/Dr17b3F/Screenshot-2023-07-13-at-2-53-56-PM.png)


![App Screenshot](https://i.ibb.co/F4sJvwT/Screenshot-2023-07-13-at-2-54-21-PM.png)

![App Screenshot](https://i.ibb.co/hK7PKSZ/Screenshot-2023-07-06-at-12-55-33-AM.png)


## DEMO VIDEO

[Electron App & Redis Chat - Demonstration][1]

[1]: https://sprinklr-my.sharepoint.com/:v:/p/pranav1/ETH8uLtGJjhEibzrQdItDh4BmzJEVVl58hN2THGCpJrahQ?e=CudiDf
## Testing

To test the tool

```bash
  npm run test
```


![Logo](https://i.ibb.co/SrVGdPY/Sprinklr-Logo-Primary-Use-Positive-RBG.jpg)

